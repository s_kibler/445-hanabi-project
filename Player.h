#ifndef PLAYER_H
#define PLAYER_H

#include "GameConstants.h"
#include "Card.h"
#include "Events.h"
#include <vector>
#include <iostream>
using namespace std;
using std::vector;

//enum Discard_Approval {UNKNOWN_DISC=-1, NO_DISC=0, YES_DISC=1};//note that yes and no are effectively bools :)

class Player
{
public:
	Player(vector<Card> startinghand);
	Player(const Player& p);
	Player();//default
	void tell(Event* e, vector<int> board, int hints, int fuses, vector<Card> oHand, int deckSize);//player is ASKING (game telling)
	Event* ask();//player is TELLING (game asking)

private:
	//goodness_param calculator functions used by ask()--each returns calculated goodness parameter value
	//note: these functions don't check for illegal values (for faster run and simplicity), so don't send them junk!
		//(hopefully it's safe to assume they won't get any, since they're private member functions)
	float play_g(int hand_index);
	float discard_g(int hand_index);
	float color_hint_g(int color);
	float number_hint_g(int number);


	float discard_core_helper(Card &relevant_card, int index);//does the big loop for discard_goodness
		//note that the returned value is NOT a complete goodness value!  just a partial one
		//takes a card/ supposed card, and the place in the hand where it lies
	vector<Card> remaining_unknown_cards(int color=-1, int number=-1);//default is -1,-1 (searches for all)
		//either parameter may be included or not
		//returns a vector of every card that was NOT in the discard and other person's hand piles
		//use to determine statistics on remaining cards' average goodness params

	Event* forced_event;//set to NULL unless an explicit hint demands a certain event to run
		//if not null, ask just uses this value directly

	//vector<Card> myhand;//the player's hand--it does NOT use this for calculations though--only 'play' moves
	vector<pair<int,int>> card_knowledge;//holds the direct knowledge gained from hints from others
		//the first is color (from GameConstants.h), and the second is number value
		//note that they CANNOT both be defined!  that wouldn;t make sense for the purpose of the function, anyway
		//note: this function would be totally unnecessary if the draw pile was visible, but that wouldn't really make sense to do
	vector<pair<int,int>> ohand_card_knowledge;//same as above, but for the other person's hand
		//must be updated at the end of ask() if a hint is created for the other player

	//vector<Discard_Approval> can_i_discard;//identifies discardable matches (default UNKNOWN_DISC)
		//that's probably not actually needed, because goodness parameters are recalculated every time 
		//and could probably supplant it

	//might need to add a set of 5 event objects and use those for passing if there are issues with going out of scope

	//each agent needs to internally memorize everything about the external world (except its own hand, of course)
	vector<int> known_board;
	int known_hints;//number of hints left
	int known_fuses;//number of fuses left
	vector<Card> known_ohand;
	int known_decksize;
	vector<Card> known_discard_pile;//because knowing exactly what's in the discard pile is key to the AI's 
		//evaluation of the goodness of a choice

	static const int hint_values[];//array of values assigned to gaining/losing a hint with [index+1] left
		//values assigned in Player.cpp--making it static here allows that to happen :)
};


const int Player::hint_values[8]={4,2.5,2,1,1,0,0,0};

//anything else we forgot to initialize?
Player::Player(vector<Card> startinghand) {
	for(int i=0; i<HAND_SIZE; i++) {
		//initialize the vectors!
		card_knowledge.push_back(*(new pair<int,int>));
		ohand_card_knowledge.push_back(*(new pair<int,int>));
		known_board.push_back(0);//only since known_board's size is the same as HAND_SIZE

		card_knowledge[i].first = -1;
		card_knowledge[i].second = -1;
		ohand_card_knowledge[i].first = -1;
		ohand_card_knowledge[i].second = -1;
	}
	forced_event = NULL;
}


//default constructor...not sure if this makes logical sense, but the driver code requires it
Player::Player() {
	for(int i=0; i<HAND_SIZE; i++) {
		//initialize the vectors!
		card_knowledge.push_back(*(new pair<int,int>));
		ohand_card_knowledge.push_back(*(new pair<int,int>));
		known_board.push_back(0);//only since known_board's size is the same as HAND_SIZE

		card_knowledge[i].first = -1;
		card_knowledge[i].second = -1;
		ohand_card_knowledge[i].first = -1;
		ohand_card_knowledge[i].second = -1;
	}
	forced_event = NULL;
}

Player::Player(const Player& p) {
	card_knowledge = p.card_knowledge;
	forced_event = p.forced_event;
	ohand_card_knowledge = p.ohand_card_knowledge;
	known_board = p.known_board;
	known_decksize = p.known_decksize;
	known_discard_pile = p.known_discard_pile;
	known_fuses = p.known_fuses;
	known_hints = p.known_hints;
	known_ohand = p.known_ohand;
}

void Player::tell(Event* e, vector<int> board, int hints, int fuses, vector<Card> oHand, int deckSize) {
	//set the simple variables equal to those of the game object (for internal reference)
	//only works if the game's state is changed before the tell function is called!
	known_board = board;
	known_hints = hints;
	known_fuses = fuses;
	known_ohand = oHand;
	known_decksize = deckSize;

	//if something was discarded or played, add that card to the known_discard_pile (for future goodness param calculations)
	if(e->getAction() == DISCARD) {
		known_discard_pile.push_back(static_cast<DiscardEvent*>(e)->c);
	}
	else if(e->getAction() == PLAY) {
		known_discard_pile.push_back(static_cast<PlayEvent*>(e)->c);
	}

	//update card_knowledge and card position in myhand as needed, according to gained information from the hint
	else if(e->getAction() == COLOR_HINT) {
		//add all the hints to the known_events
		for(int i=0; i<static_cast<ColorHintEvent*>(e)->indices.size(); i++) {
			card_knowledge[static_cast<ColorHintEvent*>(e)->indices[i]].first = static_cast<ColorHintEvent*>(e)->color;
		}
		//move cards if goodness param is + or -

		//if the hint is an explicit discard hint, set the forced_event to it
		//only indices[0]--the first hint--is explicit in a multicard hint
		//if it's a discard hint
		if(known_board[static_cast<ColorHintEvent*>(e)->color] == 5)
			forced_event = new DiscardEvent(static_cast<ColorHintEvent*>(e)->indices[0]);
		else//otherwise, it's an explicit play hint
			forced_event = new PlayEvent(static_cast<ColorHintEvent*>(e)->indices[0]);
	}

	else if(e->getAction() == NUMBER_HINT) {
		//add all the hints to the known_events
		for(int i=0; i<static_cast<NumberHintEvent*>(e)->indices.size(); i++) {
			card_knowledge[static_cast<NumberHintEvent*>(e)->indices[i]].second = static_cast<NumberHintEvent*>(e)->number;
		}
		//move cards if goodness param is + or -

		//check min and max's for future use in evaluating what the number hint meant
		int maxnum = -1;//makes loop have no exceptions
		int minum = 6;//makes loop have no exceptions
		for(int k=0; k<5; k++) {
			if(known_board[k] > maxnum) maxnum = known_board[k];
			if(known_board[k] < minum) minum = known_board[k];
		}

		//if it's an explicit discard hint then set forced to it
		//wow.  what a stinking huge piece of code
		if((static_cast<NumberHintEvent*>(e)->number <= minum) || 
			(card_knowledge[static_cast<NumberHintEvent*>(e)->indices[0]].first != -1
			&& static_cast<NumberHintEvent*>(e)->number <= known_board[card_knowledge[static_cast<NumberHintEvent*>(e)->indices[0]].first])) {
				forced_event = new DiscardEvent(static_cast<NumberHintEvent*>(e)->indices[0]);
		}
		//if it's an info hint (not playable now, but will be later), don't do anything 
			//(since play is default, this is necessary to filter the info hints out)
		else if((static_cast<NumberHintEvent*>(e)->number > maxnum+1) || 
			(card_knowledge[static_cast<NumberHintEvent*>(e)->indices[0]].first != -1
			&& static_cast<NumberHintEvent*>(e)->number > known_board[card_knowledge[static_cast<NumberHintEvent*>(e)->indices[0]].first]+1)) {
				//do nothing!  an info hint has already done its due dilligence
		}
		//otherwise, it's treated as an explicit play hint
		else forced_event = new PlayEvent(static_cast<NumberHintEvent*>(e)->indices[0]);
	}
	//the tell function needs to evaluate hints based on the info/explicit play/ explicit discard
	return;
}

Event* Player::ask() {
	int best_goodness_value_index = -1;//holds the index of the event in "unsorted_events_..." with the best goodness
	float best_goodness_value = -100000;//holds the actual value of goodness associated with the index above
		//the int is set so huge negative to be below any goodness value returned by any branch
	vector<pair<float,Event*>> unsorted_events_and_goodnesses;//holds ALL events with attached goodness params
	pair<float, Event*> temppair;//used to hold the possible events (one at a time, for entry into "unsorted_events..."
	vector<int> temp_hint_vec;//holds the indices of cards in the hand which satisfy a certain color or number
		//(depending upon where it's used)

	//four types of options (plus the forced event due to an explicit hint):
		//-play one of the cards (known or unknown? partly known? based on what's not there)
		//-discard one of the cards (same considerations as above)
		//-hint another player's COLOR or NUMBER on card(s) (playable or not, implicit hints?)
	Event* best_option;//holds the currently most-useful option

	//don't even bother checking goodness params if there was an explicit hint--just trust and go with it
	if(forced_event) best_option = forced_event;

	else {

		//combined play/ discard for loop
		for(int i=0; i<HAND_SIZE-(known_ohand.size() != HAND_SIZE); i++) {
			//the discard
			temppair.first = discard_g(i);
			temppair.second = new DiscardEvent(i);
			unsorted_events_and_goodnesses.push_back(temppair);
			//the play
			temppair.first = play_g(i);
			temppair.second = new PlayEvent(i);
			unsorted_events_and_goodnesses.push_back(temppair);
		}
		//the color hint
		for(int j=0; j<NUM_COLORS; j++) {
			temppair.first = color_hint_g(j);
			temp_hint_vec.clear();//reset from last use
			for(int f=0; f<HAND_SIZE-(known_ohand.size() != HAND_SIZE); f++) {
				if(card_knowledge[f].first == j) temp_hint_vec.push_back(f);
			}
			temppair.second = new ColorHintEvent(temp_hint_vec, j);
			unsorted_events_and_goodnesses.push_back(temppair);
		}
		//the number hint
		for(int k=0; k<NUM_NUMBERS; k++) {
			temppair.first = number_hint_g(k);
			temp_hint_vec.clear();//reset from last use
			for(int q=0; q<HAND_SIZE-(known_ohand.size() != HAND_SIZE); q++) {
				if(card_knowledge[q].second == k) temp_hint_vec.push_back(q);
			}
			temppair.second = new NumberHintEvent(temp_hint_vec, k);
			unsorted_events_and_goodnesses.push_back(temppair);
		}

		//now, evaluate the best option of all of these, and return it
		for(int p=0; p<unsorted_events_and_goodnesses.size(); p++) {
			if(unsorted_events_and_goodnesses[p].first > best_goodness_value) {
				best_goodness_value_index = p;//set new index of best event
				best_goodness_value = unsorted_events_and_goodnesses[p].first;//set goodness of new best event
			}
		}

		best_option = unsorted_events_and_goodnesses[best_goodness_value_index].second;
		//cout << "BEST GOODNESS VALUE IS " << best_goodness_value << endl;

		//this messes up the return pointers...durp.
		//tried to free memory, but no such luck
		/*for(int h=0; h<unsorted_events_and_goodnesses.size(); h++) {
		delete unsorted_events_and_goodnesses[h].second;
		}*/

	}

	//finally, remove it from its card_knowledge, because it's not there anymore!
	if(best_option->getAction() == PLAY || best_option->getAction() == DISCARD) {
		card_knowledge[static_cast<DiscardEvent*>(best_option)->position].first = -1;
		card_knowledge[static_cast<DiscardEvent*>(best_option)->position].second = -1;
	}

	//delete forced_event;
	forced_event = NULL;//reset this for next time
	return best_option;
}

float Player::play_g(int index){
	float goodness = 0;
	float avg_goodness = 0;
	Card temp_card;

	if(card_knowledge[index].first != -1 && card_knowledge[index].second != -1){
		
		if(known_board[card_knowledge[index].first] == card_knowledge[index].second -1){
			goodness += 3;

			if(card_knowledge[index].second == 5){
				goodness += hint_values[known_hints - 1];
			}
		}
		else {
			if(known_fuses == 3){
				goodness -= 100;
			}
			else if(known_fuses == 2){
				goodness -= 300;
			}
			else if(known_fuses ==1){
				goodness -= 1000;
			}
		}
	}

	else {
		
		vector<Card> my_possible_card = remaining_unknown_cards(card_knowledge[index].first, card_knowledge[index].second);

		for(int i = 0; i < my_possible_card.size(); i++){
			goodness = 0;
			if(known_board[my_possible_card[i].color] == my_possible_card[i].number - 1){
				goodness += 3;

				if(my_possible_card[i].number == 5){
					goodness += hint_values[known_hints - 1];
				}
			}
			else {
				if(known_fuses == 3){
					goodness -= 100;
				}
				else if(known_fuses == 2){
					goodness -= 300;
				}
				else if(known_fuses ==1){
					goodness -= 1000;
				}
			}
			avg_goodness += goodness;
			
		}
		avg_goodness /= my_possible_card.size();
		goodness = avg_goodness;
	}

	//finally, penalize for low-draw-pile draw for both good and bad plays
	int number_of_cards_left = 50-HAND_SIZE*2-known_discard_pile.size();
	//apply increasing penalty for causing draws once the draw pile starts to run thin
	if((number_of_cards_left < 9) && number_of_cards_left)//between 0 and 9 exclusive
		goodness -= (9-number_of_cards_left)*1.1;
	if((number_of_cards_left < 4) && number_of_cards_left)//between 0 and 3 exclusive
		goodness -= (9-number_of_cards_left)*0.6;

	return goodness;
}

//constants to modify: value per low deck draw=0.5; immediate playability=0.5; return hint value (see player.h);
	//begin low deck=8; permanently unplayable card=2
float Player::discard_g(int index) {
	float goodness = 0;//default value, from which all other value changes are applied
	Card temp_card;

	//discard value = hint value returned - cost based on number of that card left
		// - 0.5 extra penalty for immediately playable cards (but not so much to favor using rarer cards instead)
		// - 0.5*number of cards left fewer than 9 (unless there are 0 left)
		// + 2 for a card that has been passed in the board pile already (thus is forever unplayable)

	//fully known card = known goodness
	if(card_knowledge[index].first != -1 && card_knowledge[index].second != -1) {
		temp_card.color = card_knowledge[index].first;
		temp_card.number = card_knowledge[index].second;
		goodness = discard_core_helper(temp_card, index);//pass it off to a helper func to prevent redundancy below
	}


	//now, on to calculating the random and semi-random discard values
	//run the previous calculation on EVERY POSSIBLE CARD IN PLAY that fits the description!  pretty heavy stuff...
	else {
		//grab a list of every card it could possibly be
		vector<Card> possibly_my_card = remaining_unknown_cards(card_knowledge[index].first, card_knowledge[index].second);
		float avg_goodness = 0;

		//for each entry in that list, calculate its goodness
		for(int y=0; y<possibly_my_card.size(); y++) {
			goodness = 0;//reset after every iteration!
			temp_card = possibly_my_card[y];
			goodness = discard_core_helper(temp_card, index);//that big chunk of code is avoided again :)
			//append to the sum of goodness for averaging
			avg_goodness += goodness;
		}

		//make the sum an average
		avg_goodness /= possibly_my_card.size();
		goodness = avg_goodness;//finally, start using the goodness variable as it was intended again (avg of all goodness values)
	}

	//the following steps apply to both known and random cards:

	//add value of hint acquired
	goodness += hint_values[known_hints-1];

	//5 suits w/ 10 cards apiece; 5 cards each in 2 hands; whatever size the discard pile is
	int number_of_cards_left = 50-HAND_SIZE*2-known_discard_pile.size();
	//finally, apply increasing penalty for causing draws once the draw pile starts to run thin
	if((number_of_cards_left < 9) && number_of_cards_left)//between 0 and 9 exclusive
		goodness -= (9-number_of_cards_left)*1.1;
	if((number_of_cards_left < 4) && number_of_cards_left)//between 0 and 3 exclusive
		goodness -= (9-number_of_cards_left)*0.6;
	//if(number_of_cards_left == 0) goodness = -15;//DON'T DISCARD during endgame!

	return goodness;
}
float Player::discard_core_helper(Card& relevant_card, int index) {
	float goodness = 0;

	//if a card is a duplicate of one already in the hand, it needs no penalty
	bool duplicate = false;
	for(int q=0; q<HAND_SIZE-(known_ohand.size() != HAND_SIZE); q++) {
		if(q != index && card_knowledge[q].first == relevant_card.color
			&& card_knowledge[q].second == relevant_card.number)
			duplicate = true;
	}
	//if the card has already been played, it's great to remove!
	if(known_board[relevant_card.color] >= relevant_card.number)
		goodness+=2;
	//if it's playable, it's a little worse to discard it than normal
	else if(!duplicate && (known_board[relevant_card.color] == relevant_card.number-1))
		goodness-=play_g(index)/28;//between more rare card value and same rarity card value

	//ones not immediately playable, but which will be playable later, get neither bonus nor penalty

	//search the discard deck and the other person's hand to determine rarity
	//if it's in the other person's hand, then there's no problem
	if(!duplicate) {
		int discarded_duplicate_count = 0;
		//if it's a 5, then you're guaranteed it's the only one--DON'T DISCARD!
		if(relevant_card.number==5) goodness-=3.2;

		else {//add up the duplicates from the discard to determine current rarity of the card
			for(int w=0; w < known_discard_pile.size(); w++) {
				if(known_discard_pile[w].color == relevant_card.color
					&& known_discard_pile[w].number == relevant_card.number)
					discarded_duplicate_count++;
			}

			//no more left besides it--lower values = worse discard because of opportunity lost!
			if((discarded_duplicate_count==1 && relevant_card.number!=1)
				|| (discarded_duplicate_count==2)) goodness-=(3.2*(NUM_NUMBERS+1-relevant_card.number));
			//one more left besides it
			else if((discarded_duplicate_count==0 && relevant_card.number!=1)
				|| discarded_duplicate_count==1) goodness-=1;
			//else goodness stays the same, because removing the third to last one is not that bad
		}
	}

	return goodness;
}

//multicard_hint = -3000
//important note: explicit color hints are ALWAYS plays
float Player::color_hint_g(int color) {
	float goodness =0;
	int index = HAND_SIZE+1;//shouldn't leave this unset!  it'll fail if it reads -1 :)

	//eliminate multicard hints for now! (just by making them really bad goodness param value)
	//eliminates 0-card hints, also.  Those would be just dumb (and probably illegal)
	int card_count = 0;
	for(int i=0; i<HAND_SIZE-(known_ohand.size() != HAND_SIZE); i++) {
		if(known_ohand[i].color == color){
			card_count++;
			if(i<index) index = i;
		}
	}
	if(card_count == 0 || known_hints == 0) goodness -= 3000;

	//the meat of the hint function--goodness = (goodness of play caused)-(value of hint consumed)
	else /*if(ohand_card_knowledge[index].first != -1)*/ {
		//if the board's highest card for a color is 5, then it's a discard hint
		if(known_board[known_ohand[index].color] == 5) {
			goodness += 2;//permanently unplayable card
			goodness += hint_values[known_hints - 1];//return hint value
		}
		//since color hints always cause play (besides the above instance), simulate the play function on it for the next player's turn
		//note that the play will have full knowledge every time, so it only executes the 'full knowledge' portion
		else if(known_board[known_ohand[index].color] == known_ohand[index].number -1){
			goodness += 3;

			if(known_ohand[index].number == 5){
				goodness += 0.4 + hint_values[known_hints - 1];
			}
		}
		else {
			if(known_fuses == 3) goodness -= 6;
			else if(known_fuses == 2) goodness -= 9;
			else if(known_fuses ==1) goodness -= 1000;
		}

		//penalize for low-draw-pile draw for both good and bad actions--applys to both plays and discards
		int number_of_cards_left = 50-HAND_SIZE*2-known_discard_pile.size();
		//apply increasing penalty for causing draws once the draw pile starts to run thin
		if((number_of_cards_left < 9) && number_of_cards_left)//between 0 and 9 exclusive
			goodness -= (9-number_of_cards_left)*1.1;
		if((number_of_cards_left < 4) && number_of_cards_left)//between 0 and 3 exclusive
			goodness -= (9-number_of_cards_left)*0.6;
	}

	//if the other player already knew that, then it's pointless
	//else goodnwss += 0;

	//finally, subtract the value of the hint being consumed
	goodness -= hint_values[known_hints-1];

	return goodness;
}

//multicard_hint = -3000
//number_hints may indicate discard or play, depending on the circumstances
//also, hints that the receiving player cannot tell if are play or discard are automatically interpreted as play hints!
//constants to mess with: info hint = 1
//doesn't need to check for discardable based on new knowledge because it's only single-card hint (immediate use of info)
float Player::number_hint_g(int number) {
	float goodness = 0;
	int index = HAND_SIZE+1;//shouldn't leave this unset!  it'll fail if it reads -1 :)
	vector<int> more_indices;//holds the 'free info hints' indices

	//eliminate multicard hints for now! (just by making them really bad goodness param value)
	//eliminates 0-card hints, also.  Those would be just dumb, (and probably illegal)
	int card_count = 0;
	for(int i=0; i<HAND_SIZE-(known_ohand.size() != HAND_SIZE); i++) {
		if(known_ohand[i].number == number){
			card_count++;
			if(i<index) index = i;
			if(index < HAND_SIZE+1) more_indices.push_back(i);
		}
	}
	if(card_count == 0 || known_hints == 0) goodness -= 3000;

	//again, this part is the 'real' function body (not fluff)
	else /*if(ohand_card_knowledge[index].second != -1)*/ {
		int maxnum = -1;//makes loop have no exceptions
		int minum = 6;//makes loop have no exceptions
		//first, store max and min card #s in board
		for(int k=0; k<5; k++) {
			if(known_board[k] > maxnum) maxnum = known_board[k];
			if(known_board[k] < minum) minum = known_board[k];
		}

		//if it will trigger an explicit discard, use those values (general w/ nothing or particular w/ knowledge)
		if((known_ohand[index].number <= minum) || (ohand_card_knowledge[index].first != -1
			&& known_ohand[index].number <= known_board[known_ohand[index].color])) {
				//do discard on good discard (because the parameters make this always a good discard)
				goodness += 2;//base value of known good discard
				goodness += hint_values[known_hints - 1];//hint bonus

				//penalize for low-draw-pile for drawing for discards
				int number_of_cards_left = 50-HAND_SIZE*2-known_discard_pile.size();
				//apply increasing penalty for causing draws once the draw pile starts to run thin
				if((number_of_cards_left < 9) && number_of_cards_left)//between 0 and 9 exclusive
					goodness -= (9-number_of_cards_left)*1.1;
				if((number_of_cards_left < 4) && number_of_cards_left)//between 0 and 3 exclusive
					goodness -= (9-number_of_cards_left)*0.6;
		}

		//it's just an info hint (clearly unplayable, but will be later), use 'info' value
		//(general w/ nothing or particular w/ knowledge)
		else if((known_ohand[index].number > maxnum+1) || (ohand_card_knowledge[index].first != -1
			&& known_ohand[index].number > known_board[known_ohand[index].color]+1)) {
				goodness += 1;
		}

		//otherwise, it is an explicit play hint, because (hopefully) it won't hint if it's a bad play!
		else {
			
			//note that the play will have full knowledge every time, so it only executes the 'full knowledge' portion
			if(known_board[known_ohand[index].color] == known_ohand[index].number -1){
				goodness += 3;//good play

				if(known_ohand[index].number == 5){
					goodness += 0.4 + hint_values[known_hints - 1];//hint bonus; 5s are more rare, too
				}
			}
			else {//bad play
				if(known_fuses == 3) goodness -= 6;
				else if(known_fuses == 2) goodness -=9;
				else if(known_fuses ==1) goodness -= 1000;
			}
		}

		//add 'freebie points' for info hints on an explicit multicard hint
		for(int w=0; w<more_indices.size(); w++) {
			goodness += 0.1;
		}

		//penalize for low-draw-pile draw for both good and bad plays
		int number_of_cards_left = 50-HAND_SIZE*2-known_discard_pile.size();
		//apply increasing penalty for causing draws once the draw pile starts to run thin
		if((number_of_cards_left < 9) && number_of_cards_left)//between 0 and 9 exclusive
			goodness -= (9-number_of_cards_left)*1.1;
		if((number_of_cards_left < 4) && number_of_cards_left)//between 0 and 3 exclusive
			goodness -= (9-number_of_cards_left)*0.6;
	}

	//else they already knew that, so it's basically pointless
	//else goodness += 0;

	//finally, subtract the value of the hint being consumed
	goodness -= hint_values[known_hints-1];

	return goodness;
}

//this function is high O(N2) to run, so the calculations here are hefty.  luckily, depth = 1 for move checks
//(though arguably depth=2 might be better during endgame)
vector<Card> Player::remaining_unknown_cards(int color, int number) {
	vector<Card> remaining_cards;
	Card mytempcard(RED,1);

	//push back all applicable cards (yuck. hard coding needed)
	//specific number needed
	if(number!=-1) {
		mytempcard.number=number;
		for(int b=0; b<NUM_COLORS; b++) {
			mytempcard.color = b;
			remaining_cards.push_back(mytempcard);//once for 5
			if(number<5) remaining_cards.push_back(mytempcard);//twice for most others
			if(number==1) remaining_cards.push_back(mytempcard);//thrice for 1
		}
	}
	//ALL THE CAAAARDS
	else if(color==-1) {
		for(int i=0; i<NUM_COLORS; i++) {//the alternative to hard-coding would be a nested for loop using (10-num)int/3int
			mytempcard.color=i;
			for(int g=1; g<=NUM_NUMBERS; g++) {
				mytempcard.number = g;
				remaining_cards.push_back(mytempcard);//once for 5
				if(g<5) remaining_cards.push_back(mytempcard);//twice for most others
				if(g==1) remaining_cards.push_back(mytempcard);//thrice for 1
			}
		}
	}
	else {//only the single color used
		mytempcard.color=color;
		for(int g=1; g<=NUM_NUMBERS; g++) {
			mytempcard.number = g;
			remaining_cards.push_back(mytempcard);//once for 5
			if(g<5) remaining_cards.push_back(mytempcard);//twice for most others
			if(g==1) remaining_cards.push_back(mytempcard);//thrice for 1
		}
	}


	//now, remove the ones that have been discarded and are in the other player's hand
	//discarded pile check
	for(int j=0; j<known_discard_pile.size();j++) {
		vector<Card>::iterator remaincard_iter;
		for(remaincard_iter=remaining_cards.begin(); remaincard_iter!=remaining_cards.end(); remaincard_iter++) {
			if(remaincard_iter->color == known_discard_pile[j].color 
				&& remaincard_iter->number == known_discard_pile[j].number) {
					remaining_cards.erase(remaincard_iter);
					break;//the iterator will fail, but that's okay because it's reset every time
			}
		}
	}
	//other player's hand check
	for(int j=0; j<known_ohand.size();j++) {
		vector<Card>::iterator remaincard_iter;
		for(remaincard_iter=remaining_cards.begin(); remaincard_iter!=remaining_cards.end(); remaincard_iter++) {
			if(remaincard_iter->color == known_ohand[j].color 
				&& remaincard_iter->number == known_ohand[j].number) {
					remaining_cards.erase(remaincard_iter);
					break;//the iterator will fail, but that's okay because it's reset every time
			}
		}
	}


	//what's left is a vector of all the possible playable cards for a given player (finally!)
	return remaining_cards;
}

#endif