// Hanabi.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include "Player.h"
#include "Game.h"

using namespace std;

const double RUNS = 10000;

int main()
{
	int total = 0;
	Game g(false);
	for (int i = 0; i < RUNS; i++)
	{
		Player p1;
		Player p2;
		g.setup(p1, p2);
		total += g.gameLoop();
	}
	cout << "Average score " << total/RUNS << "." << endl;
	return 0;
}